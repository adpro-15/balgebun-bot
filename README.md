[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase-db/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase-db)

## Master
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/master/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/master)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/master/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/master)

###  Database Branch
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/database/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/database)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/database/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/database)

### UI/UX Branch
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/UI/UX/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/UI/UX)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/UI/UX/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/UI/UX)

### Purchase Branch
[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase)
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase)

### Promo Branch

[![pipeline status](https://gitlab.com/adpro-15/balgebun-bot/badges/promoRevised/pipeline.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/promoRevised)

[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/promoRevised/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/promoRevised)

### Menu Branch
[![coverage report](https://gitlab.com/adpro-15/balgebun-bot/badges/purchase-db/coverage.svg)](https://gitlab.com/adpro-15/balgebun-bot/-/commits/purchase-db)


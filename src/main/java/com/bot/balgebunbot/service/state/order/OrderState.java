package com.bot.balgebunbot.service.state.order;

import com.bot.balgebunbot.service.BotService;
import com.bot.balgebunbot.service.state.State;
import com.linecorp.bot.model.message.Message;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class OrderState implements State {

    @Autowired
    protected BotService botService;

    protected List<Message> responses;

    public abstract List<Message> accept(String orderId);

    public abstract List<Message> cook(String orderId);

    public abstract List<Message> finishCooking(String orderId);

    public abstract List<Message> delivered(String orderId);
}

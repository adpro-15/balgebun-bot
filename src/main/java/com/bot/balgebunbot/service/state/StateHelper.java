package com.bot.balgebunbot.service.state;

import com.bot.balgebunbot.service.BotService;
import com.bot.balgebunbot.service.state.customer.*;
import com.bot.balgebunbot.service.state.order.*;
import com.bot.balgebunbot.service.state.seller.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {

    @Autowired
    private CustomerRegisteredState customerRegisteredState;

    @Autowired
    private CustomerUnregisteredState customerUnregisteredState;

    @Autowired
    private CustomerConfirmedOrderState customerConfirmedOrderState;

    @Autowired
    private CustomerPayedState customerPayedState;

    @Autowired
    private CustomerOrderingState customerOrderingState;

    @Autowired
    private SellerRegisteredState sellerRegisteredState;

    @Autowired
    private SellerUnregisteredStatePhase1 sellerUnregisteredStatePhase1;

    @Autowired
    private SellerUnregisteredStatePhase2 sellerUnregisteredStatePhase2;

    @Autowired
    private SellerAcceptingOrderState sellerAcceptingOrderState;

    @Autowired
    private OrderFinishedState orderFinishedState;

    @Autowired
    private OrderDeliveredState orderDeliveredState;

    @Autowired
    private OrderCookingState orderCookingState;

    @Autowired
    private OrderAcceptedState orderAcceptedState;

    @Autowired
    private BotService botService;


    public OrderState orderToState(String stateString){

        switch(stateString){

            case OrderAcceptedState.DB_COL_NAME:
                return orderAcceptedState;

            case OrderDeliveredState.DB_COL_NAME:
                return orderDeliveredState;

            case OrderCookingState.DB_COL_NAME:
                return orderCookingState;

            case OrderFinishedState.DB_COL_NAME:
                return orderFinishedState;

            default:
                return null;
        }
    }

    public CustomerState customerToState(String stateString){
        switch(stateString){
            case CustomerRegisteredState.DB_COL_NAME:
                return customerRegisteredState;

            case CustomerUnregisteredState.DB_COL_NAME:
                return customerUnregisteredState;

            case CustomerOrderingState.DB_COL_NAME:
                return customerOrderingState;

            case CustomerConfirmedOrderState.DB_COL_NAME:
                return customerConfirmedOrderState;

            case CustomerPayedState.DB_COL_NAME:
                return customerPayedState;

            default:
                return null;
        }
    }

    public SellerState sellerToState(String stateString){
        switch(stateString){
            case SellerRegisteredState.DB_COL_NAME:
                return sellerRegisteredState;

            case SellerUnregisteredStatePhase1.DB_COL_NAME:
                return sellerUnregisteredStatePhase1;

            case SellerUnregisteredStatePhase2.DB_COL_NAME:
                return sellerUnregisteredStatePhase2;

            case SellerAcceptingOrderState.DB_COL_NAME:
                return sellerAcceptingOrderState;

            default:
                return null;
        }
    }

    public CustomerState getCustomerState(String userId){
        CustomerState state;
        if(botService.getCustomer(userId).isPresent()){
            state = customerToState(botService.getCustomer(userId).get().getState());
        }
        else {
            state = customerUnregisteredState;
        }
        return state;
    }

    public SellerState getSellerState(String userId) {
        SellerState state;
        if(botService.getSeller(userId).isPresent()){
            state = sellerToState(botService.getSeller(userId).get().getState());
        }
        else {
            state = sellerUnregisteredStatePhase1;
        }
        return state;
    }
}

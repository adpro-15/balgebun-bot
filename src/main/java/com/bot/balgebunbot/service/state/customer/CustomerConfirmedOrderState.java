package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.Purchase;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerConfirmedOrderState extends CustomerState {

    public static final String DB_COL_NAME = "CUSTOMER ORDERED";

    public CustomerConfirmedOrderState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_CUSTOMER);
        return responses;
    }

    @Override
    public List<Message> addOrder(String userId, String itemId, String quantity, String notes) {
        clearResponses();
        responses.add(Messages.HAS_ORDERED);
        return responses;
    }

    @Override
    public List<Message> cancel() {
        return responses;
    }


    @Override
    public List<Message> removeOrder(String purchaseId) {
        clearResponses();
        responses.add(Messages.HAS_ORDERED);
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> confirmOrder(String userId) {
        clearResponses();
        responses.add(Messages.HAS_ORDERED);
        return responses;
    }

    @Override
    public List<Message> payOrder(String userId) {
        clearResponses();
        if(botService.getCustomer(userId).isPresent()) {
            botService.getCustomer(userId).get().setState("CUSTOMER PAYED");
            for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
                if(ord.getState().equals("ORDER UNACCEPTED")){
                    ord.setState("ORDER ACCEPTED");
                    botService.updatePurchase(ord);
                }
            }
            responses.add(Messages.PAYMENT_SUCCESS);
            responses.add(new TextMessage("if /orderstatus if finished, then /accept"));
        }
        else {
            responses.add(Messages.PAYMENT_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> accept(String userId) {
        responses.add(new TextMessage("You have no order to accept!"));
        return responses;
    }

}

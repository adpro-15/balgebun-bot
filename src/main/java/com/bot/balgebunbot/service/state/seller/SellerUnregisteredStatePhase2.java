package com.bot.balgebunbot.service.state.seller;

import com.bot.balgebunbot.model.Seller;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SellerUnregisteredStatePhase2 extends SellerState {
    public static final String DB_COL_NAME = "SELLER UNREGISTERED2";

    public SellerUnregisteredStatePhase2(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        return responses;
    }

    @Override
    public List<Message> register2(String userId, String gopayNumber, String counterId) {
        clearResponses();
        if(counterId.isEmpty() || gopayNumber.isEmpty()){
            responses.add(new TextMessage("Wrong command! /register <gopay> <counterid>"));
        }
        else if(botService.getSeller(userId).isPresent()){
            Seller seller = botService.getSeller(userId).get();
            seller.setPhoneNumber(counterId);
            seller.setGopayNumber(gopayNumber);
            seller.setState(SellerRegisteredState.DB_COL_NAME);
            responses.add(new TextMessage("Success!"));
            botService.updateSeller(seller);

        }
        else {
            responses.add(Messages.INVALID);
        }
        return responses;
    }
    @Override
    public List<Message> checkPromo(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_SELLER);
        return responses;
    }

    @Override
    public List<Message> cancel() {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getOrders(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getHistory(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> addMenu(String sellerId, String price, String itemName) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> removeMenu(String itemId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> addPromo(String userId, String promo) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> removePromo(String promoId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getReview(String sellerId, String itemId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> notifyOrderDone(String orderId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> cookOrder(String orderId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }
}

package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerRegisteredState extends CustomerState {

    public static final String DB_COL_NAME = "CUSTOMER REGISTERED";

    public CustomerRegisteredState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_CUSTOMER);
        return responses;
    }

    @Override
    public List<Message> addOrder(String userId, String itemId, String quantity, String notes) {
        clearResponses();
        if(itemId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /order <itemId> optional(<quantity> <notes>"));
        }
        else if(botService.getItem(itemId).isPresent()){
            try {
                if(quantity.isEmpty()) quantity = "1";
                Item item = botService.getItem(itemId).get();
                Seller seller = item.getSeller();
                Customer customer = botService.getCustomer(userId).get();
                Purchase purchase = new Purchase(Integer.toString(botService.getAllPurchase().size()));
                purchase.setItemBought(item);
                purchase.setSeller(seller);
                purchase.setCustomer(customer);
                purchase.setNotes(notes);
                purchase.setQuantity(Long.parseLong(quantity));
                purchase.setPurchaseID(Integer.toString(botService.getAllPurchase().size()));
                purchase.setState("ORDER UNACCEPTED");
                botService.addPurchase(purchase);
                customer.setState("CUSTOMER ORDERING");
                botService.updateCustomer(customer);
                responses.add(Messages.ADD_ORDER_SUCCESS);
            }
            catch(Exception e){
                responses.add(Messages.ADD_ORDER_FAILED);
            }
        }
        else {
            responses.add(Messages.ADD_ORDER_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> cancel() {
        return responses;
    }

    @Override
    public List<Message> getOrderStatus(String userId) {
        clearResponses();
        responses.add(new TextMessage("You haven't ordered anything!"));
        return responses;
    }


    @Override
    public List<Message> removeOrder(String purchaseId) {
        clearResponses();
        responses.add(new TextMessage("You haven't ordered yet!"));
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }


    @Override
    public List<Message> confirmOrder(String userId) {
        clearResponses();
        responses.add(Messages.NO_ORDER);
        return responses;
    }


    @Override
    public List<Message> payOrder(String userId) {
        clearResponses();
        responses.add(Messages.NO_ORDER);
        return responses;
    }

    @Override
    public List<Message> accept(String userId) {
        responses.add(new TextMessage("You have no order to accept!"));
        return responses;
    }


}

package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerOrderingState extends CustomerState {

    public static final String DB_COL_NAME = "CUSTOMER ORDERING";

    public CustomerOrderingState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_CUSTOMER);
        return responses;
    }



    @Override
    public List<Message> addOrder(String userId, String itemId, String quantity, String notes) {
        clearResponses();
        if(itemId.isEmpty()){
            responses.add(new TextMessage("Wrong command! <itemId> optional(<quantity> <notes>"));
        }
        else if(botService.getItem(itemId).isPresent()){
            try {
                if(quantity.isEmpty()) quantity = "1";
                Item item = botService.getItem(itemId).get();
                Seller seller = item.getSeller();
                Customer customer = botService.getCustomer(userId).get();
                Purchase purchase = new Purchase(Integer.toString(botService.getAllPurchase().size()));
                purchase.setItemBought(item);
                purchase.setSeller(seller);
                purchase.setCustomer(customer);
                purchase.setNotes(notes);
                purchase.setQuantity(Long.parseLong(quantity));
                purchase.setState("ORDER UNACCEPTED");
                purchase.setPurchaseID(Integer.toString(botService.getAllPurchase().size()));
                botService.addPurchase(purchase);
                customer.setState("CUSTOMER ORDERING");
                botService.updateCustomer(customer);
                responses.add(Messages.ADD_ORDER_SUCCESS);
            }
            catch(Exception e){
                responses.add(Messages.ADD_ORDER_FAILED);
            }
        }
        else {
            responses.add(Messages.ADD_ORDER_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> cancel() {
        clearResponses();
        return responses;
    }

    @Override
    public List<Message> removeOrder(String purchaseId) {
        clearResponses();
        if(botService.getPurchase(purchaseId).isPresent()){
            Customer customer = botService.getPurchase(purchaseId).get().getCustomer();
            if(botService.getPurchaseFromCustomer(customer.getUsername()).isEmpty()){
                customer.setState("CUSTOMER REGISTERED");
                botService.updateCustomer(customer);
            }
            Purchase purchase = botService.getPurchase(purchaseId).get();
            purchase.setState("ORDER CANCELLED");
            botService.updatePurchase(purchase);
            responses.add(Messages.REMOVE_ORDER_SUCCESS);
        }
        else {
            responses.add(Messages.REMOVE_ORDER_FAILED);
        }
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> confirmOrder(String userId) {
        clearResponses();
        if(botService.getCustomer(userId).isPresent()){
            Customer customer = botService.getCustomer(userId).get();
            customer.setState("CUSTOMER ORDERED");
            botService.updateCustomer(customer);
            responses.add(Messages.CONFIRM_ORDER_SUCCESS);
            responses.add(new TextMessage("/pay to proceed"));
        }
        else {
            responses.add(Messages.CONFIRM_ORDER_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> payOrder(String userId) {
        clearResponses();
        responses.add(new TextMessage("You haven't confirmed your order yet! use /confirmorder to confirm your order"));
        return responses;
    }

    @Override
    public List<Message> accept(String userId) {
        responses.add(new TextMessage("You have no order to accept!"));
        return responses;
    }

    public List<Message> getOrderStatus(String userId) {
        clearResponses();
        String temp = "";
        for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
            if(ord.getState().equals("ORDER UNACCEPTED")){
                temp += ord.getPurchaseID() +": "+ ord.getItemBought().getItemName()+";"+ord.getQuantity()+";"+ord.getTotalPrice()+"\n"+ord.getState();
            }
        }
        responses.add(new TextMessage(temp));
        responses.add(new TextMessage("Use /confirmorder to confirm your order or /remove orderdId to remove your order!"));
        return responses;
    }

}

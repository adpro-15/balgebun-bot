package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerPayedState extends CustomerState {

    public static final String DB_COL_NAME = "CUSTOMER PAYED";

    public CustomerPayedState(){
        this.responses =  new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_CUSTOMER);
        return responses;
    }

    @Override
    public List<Message> addOrder(String userId, String itemId, String quantity, String notes) {
        clearResponses();
        responses.add(new TextMessage("Has payed!"));
        return responses;
    }

    @Override
    public List<Message> cancel() {
        clearResponses();
        return responses;
    }

    @Override
    public List<Message> getOrderStatus(String userId) {
        clearResponses();
        String temp = "";
        for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
            if(!ord.getState().equals("ORDER UNACCEPTED") && !ord.getState().equals("ORDER DELIVERED")){
                temp += ord.getPurchaseID() +": "+ ord.getItemBought().getItemName()+";"+ord.getQuantity()+";"+ord.getTotalPrice()+"\n"+ord.getState();
            }
        }
        responses.add(new TextMessage(temp));
        return responses;
    }


    @Override
    public List<Message> removeOrder(String purchaseId) {
        clearResponses();
        responses.add(Messages.HAS_PAYED);
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> confirmOrder(String userId) {
        clearResponses();
        responses.add(Messages.HAS_PAYED);
        return responses;
    }

    @Override
    public List<Message> payOrder(String userId) {
        clearResponses();
        responses.add(Messages.HAS_PAYED);
        return responses;
    }

    public List<Message> accept(String userId){
        clearResponses();
        String temp = "";
        if(botService.getCustomer(userId).isPresent()) {
            for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
                if(ord.getState().equals("ORDER FINISHED")){
                    Customer customer = botService.getCustomer(userId).get();
                    customer.setState("CUSTOMER REGISTERED");
                    botService.updateCustomer(customer);
                    ord.setState("ORDER DELIVERED");
                    botService.updatePurchase(ord);
                    temp = "order recieved!";
                }
            }
            if(temp.isEmpty()){
                temp = "You can't accept the order yet! The seller has yet to finish it";
            }
        }
        else {
            temp = "failed";
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

}

package com.bot.balgebunbot.service.state.seller;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SellerRegisteredState extends SellerState {

    public static final String DB_COL_NAME = "SELLER REGISTERED";

    public SellerRegisteredState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> register2(String userId, String gopayNumber, String counterId) {
        clearResponses();
        responses.add(Messages.HAS_REGISTERED);
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_SELLER);
        return responses;
    }

    @Override
    public List<Message> cancel() {
        return responses;
    }

    @Override
    public List<Message> getOrders(String userId) {
        clearResponses();
//        List<Purchase> order = botService.getPurchaseFromSeller(userId);
        List<Purchase> order = botService.getAllPurchase();
        String temp = "OrderId:status;item;quantity;notes\n";
        for(Purchase ord : order) {
            if (!ord.getState().equals("ORDER UNACCEPTED") && ord.getSeller().getUsername().equals(userId)) {
                temp += ord.getPurchaseID() + ": "+ord.getState()+";" + ord.getItemBought().getItemName() + ";" + ord.getQuantity() + ";" + ord.getNotes() + "\n";
            }
        }
        Message message = new TextMessage(temp);
        responses.add(message);
        return responses;
    }

    @Override
    public List<Message> getHistory(String userId) {
        clearResponses();
//        List<Purchase> order = botService.getPurchaseFromSeller(userId);
        Seller seller = botService.getSeller(userId).get();
        List<Purchase> order = botService.getAllPurchase();
        String temp = "";
        for(Purchase ord : order) {
            if (!ord.getState().equals("ORDER CANCELLED") && ord.getSeller().getCounterID().equals(seller.getCounterID())) {
                temp += ord.getCustomer().getName() + ": " + ord.getItemBought().getItemName() + ";" + ord.getQuantity() + ";" + ord.getNotes() + "\n";
            }
        }
        if(temp.isEmpty()){
            temp = "No history!";
        }
        Message message = new TextMessage(temp);
        responses.add(message);
        return responses;
    }

    @Override
    public List<Message> addMenu(String sellerId, String price, String itemName) {
        clearResponses();
        if(price.isEmpty() || itemName.isEmpty()){
            responses.add(new TextMessage("Wrong command! /addmenu <price> <name>"));
        }
        else if(botService.getSeller(sellerId).isPresent()){
            Seller seller = null;
            Item item = new Item();
            String itemId = Integer.toString(botService.getAllItem().size());
            item.setItemName(itemName);
            item.setPrice(Long.parseLong(price));
            item.setSeller(botService.getSeller(sellerId).get());
            item.setItemID(itemId);
            botService.addItem(item);
            responses.add(Messages.ADD_ITEM_SUCCESS);
        }
        else {
            responses.add(Messages.ADD_ITEM_FAILED);
        }
        return responses;
    }


    @Override
    public List<Message> removeMenu(String itemId) {
        clearResponses();
        if(itemId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /removemenu itemID"));
        }
        if(botService.getItem(itemId).isPresent()) {
            botService.deleteItem(itemId);
            responses.add(new TextMessage("Remove menu successful!"));
        }
        else{
            responses.add(Messages.INVALID);
        }
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> addPromo(String userId, String promoDesc) {
        clearResponses();
        if(promoDesc.isEmpty()){
            responses.add(new TextMessage("Wrong command! /addpromo <promoDesc>"));
        }
        else if(botService.getSeller(userId).isPresent()){
            Promo promo = new Promo();
            promo.setSeller(botService.getSeller(userId).get());
            promo.setPromoDescription(promoDesc);
            promo.setPromoID(Integer.toString(botService.getAllPromo().size()));
            botService.addPromo(promo);
            botService.notifyPromoToSubscriber(userId,promo);
            responses.add(Messages.ADD_PROMO_SUCCESS);
        }
        else{
            responses.add(Messages.ADD_PROMO_FAILED);
        }
        return responses;
    }

    public List<Message> checkPromo(String userId){
        clearResponses();
        String temp = "";
        Seller seller = botService.getSeller(userId).get();
        for(Promo promo : botService.getAllPromo()){
            if(promo.getSeller().getCounterID().equals(seller.getCounterID())){
                temp += promo.getPromoID() + ": " +promo.getPromoDescription() + "\n";
            }
        }
        if(temp.isEmpty()){
            temp = "You haven't made any promos! use /addpromo";
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    @Override
    public List<Message> removePromo(String promoId) {
        clearResponses();
        if(promoId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /removepromo <promoId>"));
        }
        if(botService.getPromo(promoId).isPresent()){
            botService.deletePromo(promoId);
            responses.add(Messages.REMOVE_PROMO_SUCCESS);
        }
        else {
            responses.add(Messages.REMOVE_PROMO_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> getReview(String sellerId, String itemId) {
        clearResponses();
        String temp = "";
        if(itemId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /getreview <itemId>"));
        }
        else if(botService.getItem(itemId).isPresent()){
            if(botService.getAllReview().isEmpty()){
                temp = "Nobody has reviewed this yet!";
            }
            else{
                for(Review rev : botService.getAllReview()){
                    if(rev.getItem().getItemID().equals(itemId)){
                        temp += rev.getReviewDescription() + "\n";
                    }
                }
            }

        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    @Override
    public List<Message> notifyOrderDone(String orderId) {
        clearResponses();
        if(orderId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /done <orderId>"));
        }
        else if(botService.getPurchase(orderId).isPresent()){
            Purchase purchase = botService.getPurchase(orderId).get();
            purchase.setState("ORDER FINISHED");
            botService.updatePurchase(purchase);
            responses.add(new TextMessage("Order is finished!"));
        }
        else {
            responses.add(new TextMessage("Failed!"));
        }
        return responses;
    }

    @Override
    public List<Message> cookOrder(String orderId) {
        clearResponses();
        if(orderId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /cook <orderId>"));
        }
        else if (botService.getPurchase(orderId).isPresent()){
            Purchase purchase = botService.getPurchase(orderId).get();
            purchase.setState("ORDER COOKING");
            botService.updatePurchase(purchase);
            responses.add(new TextMessage("Cooking order!"));
        }
        else {
            responses.add(new TextMessage("Cooking order failed!"));
        }
        return responses;
    }
}

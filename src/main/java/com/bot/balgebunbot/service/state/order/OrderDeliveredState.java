package com.bot.balgebunbot.service.state.order;

import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderDeliveredState extends OrderState{

    public static final String DB_COL_NAME = "ORDER DELIVERED";

    public OrderDeliveredState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> accept(String orderId) {
        return responses;
    }

    @Override
    public List<Message> cook(String orderId) {
        return responses;
    }

    @Override
    public List<Message> finishCooking(String orderId) {
        return responses;
    }

    @Override
    public List<Message> delivered(String orderId) {
        return responses;
    }
}

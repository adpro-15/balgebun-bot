package com.bot.balgebunbot.service.state.order;

import com.linecorp.bot.model.message.Message;

import java.util.List;

public class OrderUnacceptedState extends OrderState {
    @Override
    public List<Message> accept(String orderId) {
        if(botService.getPurchase(orderId).isPresent()){
            botService.getPurchase(orderId).get().setState("ORDER ACCEPTED");
        }
        return responses;
    }

    @Override
    public List<Message> cook(String orderId) {
        return null;
    }

    @Override
    public List<Message> finishCooking(String orderId) {
        return null;
    }

    @Override
    public List<Message> delivered(String orderId) {
        return null;
    }
}

package com.bot.balgebunbot.service.state.seller;

import com.bot.balgebunbot.model.Promo;
import com.bot.balgebunbot.model.Purchase;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SellerAcceptingOrderState extends SellerState {

    public static final String DB_COL_NAME = "SELLER ACCEPTING ORDER";

    public SellerAcceptingOrderState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        return responses;
    }

    @Override
    public List<Message> register2(String userId, String gopayNumber, String counterId) {
        return null;
    }

    @Override
    public List<Message> help() {
        return responses;
    }

    @Override
    public List<Message> cancel() {
        return responses;
    }

    @Override
    public List<Message> getOrders(String userId) {
        List<Purchase> order = botService.getPurchaseFromSeller(userId);
        String temp = "";
        for(Purchase ord : order){
            temp += ord.getCustomer() + " " + ord.getItemBought() + " " + ord.getQuantity() + " " + ord.getNotes() + "\n";
        }
        Message message = new TextMessage(temp);
        responses.add(message);
        return responses;
    }
    @Override
    public List<Message> checkPromo(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getHistory(String userId) {
        return responses;
    }

    @Override
    public List<Message> addMenu(String sellerId, String price, String itemName) {
        return responses;
    }

    @Override
    public List<Message> removeMenu(String itemId) {
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> addPromo(String userId, String promoDesc) {
        // Seller seller = null;
        Promo promo = new Promo();
        if(botService.getSeller(userId).isPresent()){
            promo.setSeller(botService.getSeller(userId).get());
            promo.setPromoDescription(promoDesc);
            botService.addPromo(promo);
            botService.notifyPromoToSubscriber(userId,promo);
            responses.add(Messages.ADD_PROMO_SUCCESS);
        }
        else{
            responses.add(Messages.ADD_PROMO_FAILED);
        }
        return responses;
    }

    @Override
    public List<Message> removePromo(String promoId) {
        return responses;
    }

    @Override
    public List<Message> getReview(String sellerId, String itemId) {
        return responses;
    }

    @Override
    public List<Message> notifyOrderDone(String orderId) {
        return responses;
    }

    @Override
    public List<Message> cookOrder(String orderId) {
        return responses;
    }
}

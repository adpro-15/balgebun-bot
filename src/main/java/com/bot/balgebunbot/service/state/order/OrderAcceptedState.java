package com.bot.balgebunbot.service.state.order;

import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderAcceptedState extends OrderState {

    public static final String DB_COL_NAME = "ORDER ACCEPTED";

    public OrderAcceptedState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> accept(String orderId) {
        return responses;
    }

    @Override
    public List<Message> cook(String orderId) {
        if(botService.getPurchase(orderId).isPresent()){
            botService.getPurchase(orderId).get().setState("ORDER COOKING");
        }
        return responses;
    }

    @Override
    public List<Message> finishCooking(String orderId) {
        return responses;
    }

    @Override
    public List<Message> delivered(String orderId) {
        return responses;
    }
}

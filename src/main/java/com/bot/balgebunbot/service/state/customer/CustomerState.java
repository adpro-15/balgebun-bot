package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.service.BotService;
import com.bot.balgebunbot.service.state.Messages;
import com.bot.balgebunbot.service.state.State;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class CustomerState implements State {

    @Autowired
    protected BotService botService;

    protected List<Message> responses;

    public abstract List<Message> register(String userType, String phoneNumber, String userId, String displayName);

    public abstract List<Message> help();

    public abstract List<Message> addOrder(String userId, String itemId, String quantity, String notes);

    public abstract List<Message> cancel();

    public List<Message> getOrderStatus(String userId) {
        clearResponses();
        String temp = "";
        for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
            if(ord.getState().equals("ORDER UNACCEPTED")){
                temp += ord.getPurchaseID() +": "+ ord.getItemBought().getItemName()+";"+ord.getQuantity()+";"+ord.getTotalPrice()+"\n"+ord.getState();
            }
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    public List<Message> getHistory(String userId) {
        String temp = "";
        clearResponses();
        for(Purchase ord : botService.getPurchaseFromCustomer(userId)){
            temp += ord.getPurchaseID() + " " + ord.getItemBought().getItemName() + " " + ord.getTotalPrice() + "\n";
        }
        if(temp.isEmpty()){
            temp = "No history! Start ordering!";
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    public abstract List<Message> removeOrder(String purchaseId);

    public abstract void clearResponses();

    public List<Message> addReview(String review, String itemId) {
        clearResponses();
        if(review.isEmpty() || itemId.isEmpty()){
            responses.add(new TextMessage("Wrong commnand! /review <review> <itemId>"));
        }
        else if(botService.getItem(itemId).isPresent()){
            Review review1 = new Review();
            review1.setItem(botService.getItem(itemId).get());
            review1.setReviewDescription(review);
            review1.setReviewID(Integer.toString(botService.getAllReview().size()));
            botService.addReview(review1);
            responses.add(Messages.ADD_REVIEW_SUCCESS);
        }
        else {
            responses.add(Messages.ADD_REVIEW_FAILED);
        }
        return responses;
    }

    public abstract List<Message> confirmOrder(String userId);

    public abstract List<Message> payOrder(String userId);

    public List<Message> sellerList() {
        clearResponses();
        String sellers = "CounterID: Name;Gopay\n";
        for(Seller seller : botService.getAllSeller()){
            sellers += seller.getCounterID() + ": " + seller.getName()+ ";" + seller.getGopayNumber() + "\n";
        }
        responses.add(new TextMessage(sellers));
        return responses;
    }

    public List<Message> itemList(String counterId){
        clearResponses();
        String items = "ItemID;ItemName\n";
        for(Item item: botService.getAllItem()){
            if(item.getSeller().getCounterID().equals(counterId)){
                items += item.getItemID() + ";" + item.getItemName()+ "\n";
            }
        }
        responses.add(new TextMessage(items));
        return responses;
    }

    public List<Message> invalid(){
        responses.clear();
        responses.add(Messages.INVALID);
        return responses;
    }

    public List<Message> checkPromo(String userId){
        clearResponses();
        String temp = "";
        for(Promo promo : botService.getCustomer(userId).get().getPromos()){
            temp += promo.getSeller().getCounterID() + ": " +promo.getPromoDescription() + "\n";
        }
        if(temp.isEmpty()){
            temp = "There are currently no promos available!";
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    public List<Message> subList(String userId){
        clearResponses();
        String temp = "";
        for(Seller seller :  botService.getCustomer(userId).get().getSellers()){
            temp += seller.getCounterID() + "\n";
        }
        if(temp.isEmpty()){
            temp = "You are not subscribed to anyone! use /subscribe";
        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    public List<Message> subscribe(String customerId, String counterId) {
        clearResponses();
        if(counterId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /subscribe <sellerId>"));
        }
        for(Seller seller : botService.getAllSeller()){
            if(seller.getCounterID().equals(counterId)){
                botService.subscribe(seller.getUsername(), customerId);
                responses.add(Messages.SUBSCRIBE_SUCCESS);
            }
        }
        if(responses.isEmpty()){
            responses.add(Messages.SUBSCRIBE_FAILED);
        }
        return responses;
    }

    public List<Message> unsubscribe(String customerId, String counterId) {
        clearResponses();
        if(counterId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /subscribe <sellerId>"));
        }
        for(Seller seller : botService.getAllSeller()){
            if(seller.getCounterID().equals(counterId)){
                botService.unsubscribe(seller.getUsername(), customerId);
                responses.add(new TextMessage("Unsub success"));
            }
            else {
                responses.add(new TextMessage("Unsub failed"));
            }
        }
        return responses;
    }

    public List<Message> getReview(String sellerId, String itemId) {
        clearResponses();
        String temp = "";
        if(itemId.isEmpty()){
            responses.add(new TextMessage("Wrong command! /getreview <itemId>"));
        }
        else if(botService.getItem(itemId).isPresent()){
            if(botService.getAllReview().isEmpty()){
                temp = "Nobody has reviewed this yet!";
            }
            else{
                for(Review rev : botService.getAllReview()){
                    if(rev.getItem().getItemID().equals(itemId)){
                        temp += rev.getReviewDescription() + "\n";
                    }
                }
            }

        }
        responses.add(new TextMessage(temp));
        return responses;
    }

    public abstract List<Message> accept(String userId);
}

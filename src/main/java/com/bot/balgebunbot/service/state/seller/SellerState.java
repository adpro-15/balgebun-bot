package com.bot.balgebunbot.service.state.seller;

import com.bot.balgebunbot.model.Item;
import com.bot.balgebunbot.service.BotService;
import com.bot.balgebunbot.service.state.Messages;
import com.bot.balgebunbot.service.state.State;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class SellerState implements State {

    @Autowired
    protected BotService botService;

    protected List<Message> responses;

    public abstract List<Message> register(String userType, String phoneNumber, String userId, String displayName);

    public abstract List<Message> register2(String userId, String gopayNumber, String counterId);

    public abstract List<Message> help();

    public abstract List<Message> cancel();

    public abstract List<Message> getOrders(String userId);

    public abstract List<Message> getHistory(String userId);

    public abstract List<Message> addMenu(String sellerId, String price, String itemName);

    public abstract List<Message> removeMenu(String itemId);

    public abstract void clearResponses();

    public abstract List<Message> addPromo(String userId, String promo);

    public abstract List<Message> removePromo(String promoId);

    public abstract List<Message> getReview(String sellerId, String itemId);

    public abstract List<Message> notifyOrderDone(String orderId);
    public abstract List<Message> checkPromo(String userId);

    public abstract List<Message> cookOrder(String orderId);

    public List<Message> menu(String userId){
        String counterId = botService.getSeller(userId).get().getCounterID();
        clearResponses();
        String items = "ItemID;ItemName\n";
        if(botService.getAllItem().isEmpty()){
            items = "You have not add any item yet! use /addmenu";
        }
        else{
            for(Item item: botService.getAllItem()){
                if(item.getSeller().getCounterID().equals(counterId)){
                    items += item.getItemID() + ";" + item.getItemName()+ "\n";
                }
            }
        }
        responses.add(new TextMessage(items));
        return responses;
    }

    public List<Message> invalid(){
        responses.clear();
        responses.add(Messages.INVALID);
        return responses;
    }

    public List<Message> help(String userId){
        responses.clear();
        responses.add(new TextMessage("To register as customer use\n/register customer <phonenumber>\nTo register as seller use\n/register seller <counterId>"));
        return responses;
    }

}

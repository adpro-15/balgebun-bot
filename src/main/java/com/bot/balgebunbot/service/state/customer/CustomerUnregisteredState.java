package com.bot.balgebunbot.service.state.customer;

import com.bot.balgebunbot.model.Customer;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerUnregisteredState extends CustomerState {

    public static final String DB_COL_NAME ="CUSTOMER UNREGISTERED";

    public CustomerUnregisteredState(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String phoneNumber, String userId, String displayName) {
        clearResponses();
        if(userType.equals("") || phoneNumber.equals("")){
            responses.add(new TextMessage("The correct command is /register customer <phonenumber>"));
        }
        else if(userType.equals(("customer").toLowerCase())){
            if(botService.getCustomer(userId).isPresent()){
                responses.add(Messages.REGISTER_FAILED);
            }
            else {
                botService.addCustomer(new Customer(userId, displayName, phoneNumber));
                responses.add(Messages.REGISTER_SUCCESS);
            }
        }
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_CUSTOMER);
        return responses;
    }

    @Override
    public List<Message> addOrder(String userId, String itemId, String quantity, String notes) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> cancel() {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getOrderStatus(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getHistory(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> removeOrder(String purchaseId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> subscribe(String customerId, String counterId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> addReview(String review, String itemId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> confirmOrder(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> payOrder(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> accept(String userId) {
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> subList(String userId){
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> checkPromo(String userId){
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> unsubscribe(String customerId, String counterId){
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }
}

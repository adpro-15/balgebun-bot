package com.bot.balgebunbot.service.state.seller;

import com.bot.balgebunbot.model.Seller;
import com.bot.balgebunbot.service.state.Messages;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SellerUnregisteredStatePhase1 extends SellerState {

    public static final String DB_COL_NAME = "SELLER UNREGISTERED1";

    public SellerUnregisteredStatePhase1(){
        this.responses = new ArrayList<>();
    }

    @Override
    public List<Message> register(String userType, String counterId, String userId, String displayName) {
        clearResponses();
        if(userType.equals("") || counterId.equals("")){
            responses.add(new TextMessage("The correct command is /register seller <counterId>"));
        }
        if(userType.equals(("seller").toLowerCase())){
            if(botService.getSeller(userId).isPresent()){
                responses.add(Messages.INVALID);
            }
            else {
                Seller seller = new Seller(userId, displayName, "", counterId, "");
                seller.setState(SellerUnregisteredStatePhase2.DB_COL_NAME);
                botService.addSeller(seller);
                responses.add(new TextMessage("Success! use /register <gopay> <phone> to finish register"));
            }
        }
        return responses;
    }

    @Override
    public List<Message> register2(String userId, String gopayNumber, String counterId) {
        clearResponses();
        return responses;
    }

    @Override
    public List<Message> help() {
        clearResponses();
        responses.add(Messages.HELP_SELLER);
        return responses;
    }

    @Override
    public List<Message> cancel() {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getOrders(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getHistory(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> addMenu(String sellerId, String price, String itemName) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> removeMenu(String itemId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public void clearResponses() {
        this.responses.clear();
    }

    @Override
    public List<Message> addPromo(String userId, String promo) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> removePromo(String promoId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> getReview(String sellerId, String itemId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> notifyOrderDone(String orderId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> checkPromo(String userId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }

    @Override
    public List<Message> cookOrder(String orderId) {
        clearResponses();
        responses.add(Messages.INVALID);
        return responses;
    }
}

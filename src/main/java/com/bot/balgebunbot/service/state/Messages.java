package com.bot.balgebunbot.service.state;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class Messages {

    public static final Message HELP_CUSTOMER = new TextMessage("/help\n/order <itemId> optional(<quantity> <notes>)\n/sellerlist\n/item <counterId>\n/confirmorder\n/pay\n/orderstatus\n/remove <orderId>\n/history\n/review <review> <itemId>\n/accept (accept the finished order)\n/subscribe <counterId> (to get promos)\n/unsub <counterId>\n/checkpromo\n/getreview <itemId>");

    public static final Message HELP_SELLER = new TextMessage("/help\n/addmenu <price> <name>\n/cook <orderId>\n/done <orderId>\n/orders\n/menu\n/getreview <itemId>\n/history\n/addpromo <promodesc>\n/checkpromo");

    public static final Message HAS_REGISTERED = new TextMessage("You already registered!");

    public static final Message INVALID = new TextMessage("Invalid command! Please type /help to show all valid command");

    public static final Message ADD_ORDER_FAILED = new TextMessage("Add order failed!");

    public static final Message ADD_ORDER_SUCCESS = new TextMessage("Add order success!");

    public static final Message REMOVE_ORDER_SUCCESS = new TextMessage("Remove order success!");

    public static final Message REMOVE_ORDER_FAILED = new TextMessage("Remove order failed!");

    public static final Message PAYMENT_FAILED = new TextMessage("Payment failed!");

    public static final Message PAYMENT_SUCCESS = new TextMessage("Payment success!");

    public static final Message REGISTER_SUCCESS = new TextMessage("Register success!");

    public static final Message REGISTER_FAILED = new TextMessage("Register failed!");

    public static final Message SHOP_OPENED = new TextMessage("[PH]");

    public static final Message SHOP_FAILED = new TextMessage("[PH]");

    public static final Message ADD_ITEM_SUCCESS = new TextMessage("Add item success!");

    public static final Message ADD_ITEM_FAILED = new TextMessage("Add item failed!");

    public static final Message ORDER_ACCEPTED = new TextMessage("Order has been accepted!");

    public static final Message ORDER_COOKING = new TextMessage("Order is being cooked!");

    public static final Message ORDER_FINISHED = new TextMessage("Order has finished!");

    public static final Message ORDER_DELIVER = new TextMessage("Order has been delivered");

    public static final Message ADD_PROMO_SUCCESS = new TextMessage("Add promo success!");

    public static final Message ADD_PROMO_FAILED = new TextMessage("Add promo failed");

    public static final Message REMOVE_PROMO_SUCCESS = new TextMessage("Remove promo success!");

    public static final Message REMOVE_PROMO_FAILED = new TextMessage("Remove promo failed!");

    public static final Message CONFIRM_ORDER_SUCCESS = new TextMessage("Order confirmed!");

    public static final Message CONFIRM_ORDER_FAILED = new TextMessage("Order failed to confirmed!");

    public static final Message SUBSCRIBE_SUCCESS = new TextMessage("Subscribe success");

    public static final Message SUBSCRIBE_FAILED = new TextMessage("Subscribe failed");

    public static final Message NO_HISTORY = new TextMessage("[PH]");

    public static final Message NO_ORDER = new TextMessage("You haven't order anything yet! use /order to order");

    public static final Message ADD_REVIEW_SUCCESS = new TextMessage("Add review success");

    public static final Message ADD_REVIEW_FAILED = new TextMessage("Add review failed");

    public static final Message HAS_PAYED = new TextMessage("You already payed for your order!");

    public static final Message HAS_ORDERED = new TextMessage("You already ordered!");

}

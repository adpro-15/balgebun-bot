package com.bot.balgebunbot.service;
import com.bot.balgebunbot.model.*;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class FetchService {
    private HttpHeaders headers;
    private Gson gson = new Gson();
    public static RestTemplate rest = new RestTemplate();
    public static final String API_URL_Customer = "http://blgbnrest.herokuapp.com/database/customer/";
    public static final String API_URL_Seller = "http://blgbnrest.herokuapp.com/database/seller/";
    public static final String API_URL_Item = "http://blgbnrest.herokuapp.com/database/item/";
    public static final String API_URL_Review = "http://blgbnrest.herokuapp.com/database/review/";
    public static final String API_URL_Purchase = "http://blgbnrest.herokuapp.com/database/purchase/";

    public Customer findCustomerById(String customerId) throws InterruptedException {
        headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity <String> entity = new HttpEntity<String>(headers);
        Customer customer = null;
        try{
            customer = rest.getForObject(API_URL_Customer+customerId, Customer.class);
        }
        catch (Exception e){
            System.out.println("There no such customer with id: " + customerId);
        }
        return customer;
    }

    public Item findItemById(String itemId) throws InterruptedException {
        headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity <String> entity = new HttpEntity<String>(headers);
        Item item = null;
        try{
            item = rest.getForObject(API_URL_Item+itemId, Item.class);
        }
        catch (Exception e){
            System.out.println("There no such item with id: " + itemId);
        }
        return item;
    }

    public Review findReviewById(String reviewId) throws InterruptedException {
        headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity <String> entity = new HttpEntity<String>(headers);
        Review review = null;
        try{
            review = rest.getForObject(API_URL_Review+reviewId, Review.class);
        }
        catch (Exception e){
            System.out.println("There no such review with id: " + reviewId);
        }
        return review;
    }

    public Purchase findPurchaseById(String purchaseId) throws InterruptedException {
        headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity <String> entity = new HttpEntity<String>(headers);
        Purchase purchase = null;
        try{
            purchase = rest.getForObject(API_URL_Purchase+purchaseId, Purchase.class);
        }
        catch (Exception e){
            System.out.println("There no such purchase with id: " + purchaseId);
        }
        return purchase;
    }

    public Seller findSellerById(String sellerId) throws InterruptedException{
        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity <String> entity = new HttpEntity<String>(headers);
        Seller seller = null;
        try{
            seller = rest.getForObject(API_URL_Seller+sellerId, Seller.class);
        }
        catch (Exception e){
            System.out.println("There no such seller with id: " + sellerId);
        }
        return seller;
    }

    public Customer createCustomer(String username, String name, String phoneNumber, String state) {
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject customerJSONObject = new JSONObject();
        customerJSONObject.put("username", username);
        customerJSONObject.put("name", name);
        customerJSONObject.put("phoneNumber", phoneNumber);
        customerJSONObject.put("state", state);

        HttpEntity<String> request =
                new HttpEntity<String>(customerJSONObject.toString(), headers);
        Customer customer = rest.postForObject(API_URL_Customer, request, Customer.class);
        return customer;
    }

    public Seller createSeller(String username, String name, String phoneNumber, String state, String counterID
            , String gopayNumber) {
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject sellerJSONObject = new JSONObject();
        sellerJSONObject.put("username", username);
        sellerJSONObject.put("name", name);
        sellerJSONObject.put("phoneNumber", phoneNumber);
        sellerJSONObject.put("state", state);
        sellerJSONObject.put("counterID", counterID);
        sellerJSONObject.put("gopayNumber", gopayNumber);

        HttpEntity<String> request =
                new HttpEntity<String>(sellerJSONObject.toString(), headers);
        Seller seller = rest.postForObject(API_URL_Seller, request, Seller.class);
        return seller;
    }

    public Item createItem (long itemNum, String itemID, String itemName, long price, Seller seller)  {
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject itemJSONObject = new JSONObject();
        String sellerString = gson.toJson(seller);
        itemJSONObject.put("itemNum", itemNum);
        itemJSONObject.put("itemID", itemID);
        itemJSONObject.put("itemName", itemName);
        itemJSONObject.put("price", price);
        itemJSONObject.put("seller", sellerString);

        HttpEntity<String> request =
                new HttpEntity<String>(itemJSONObject.toString(), headers);
        Item item = rest.postForObject(API_URL_Item, request, Item.class);
        return item;
    }

    public Review createReview(String reviewID, String reviewDescription, Item item) {
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject reviewJSONObject = new JSONObject();
        String itemString = gson.toJson(item);
        reviewJSONObject.put("reviewID", reviewID);
        reviewJSONObject.put("reviewDescription", reviewDescription);
        reviewJSONObject.put("item", itemString);

        HttpEntity<String> request =
                new HttpEntity<String>(reviewJSONObject.toString(), headers);
        Review review = rest.postForObject(API_URL_Review, request, Review.class);
        return review;
    }

    public Purchase createPurchase(long purchaseID, long totalPrice, String notes, Seller seller, Customer customer) {
        headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject puchaseJSONObject = new JSONObject();
        String sellerString = gson.toJson(seller);
        String customerString = gson.toJson(customer);

        puchaseJSONObject.put("purchaseID", purchaseID);
        puchaseJSONObject.put("totalPrice", totalPrice);
        puchaseJSONObject.put("notes", notes);
        puchaseJSONObject.put("seller", sellerString);
        puchaseJSONObject.put("customer", customerString);

        HttpEntity<String> request =
                new HttpEntity<String>(puchaseJSONObject.toString(), headers);
        Purchase purchase = rest.postForObject(API_URL_Purchase, request, Purchase.class);
        return purchase;
    }

    public static void main(String[] args) throws Exception{
        FetchService fetchService = new FetchService();
        Customer c = fetchService.findCustomerById("kucing");
        System.out.println(c.getPhoneNumber());
        Customer newCustomer = fetchService.createCustomer("monyad", "zirka", "087777535288",  "bum");
    }
}

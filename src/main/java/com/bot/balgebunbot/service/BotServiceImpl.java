package com.bot.balgebunbot.service;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Set;
import java.util.List;
import java.util.Optional;

@Service
public class BotServiceImpl implements BotService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private SellerRepository sellerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private PromoRepository promoRepository;

    @Override
    public Optional<Customer> getCustomer(String id) {
        return customerRepository.findById(id);
    }

    @Override
    public Optional<Item> getItem(String itemID) {
        return itemRepository.findById(itemID);
    }

    @Override
    public Optional<Seller> getSeller(String id) {
        return sellerRepository.findById(id);
    }

    @Override
    public Optional<Review> getReview(String reviewID) {
        return reviewRepository.findById(reviewID);
    }

    @Override
    public Optional<Purchase> getPurchase(String purchaseID){
        return purchaseRepository.findById(purchaseID);
    }

//    @Override
//    public Optional<Payment> getPayment(String paymentID){
//        return paymentRepository.findById(paymentID);
//    }

    @Override
    public Item addItem(Item item) {
        itemRepository.save(item);
        return item;
    }

    @Override
    public Seller addSeller(Seller seller) {
        sellerRepository.save(seller);
        return seller;
    }

    @Override
    public Customer addCustomer(Customer customer) {
        customerRepository.save(customer);
        return customer;
    }

    @Override
    public Review addReview(Review review) {
        reviewRepository.save(review);
        return review;
    }

    @Override
    public void deleteCustomer(String id) {
        customerRepository.deleteById(id);
    }

    @Override
    public void deleteSeller(String id) {
        sellerRepository.deleteById(id);
    }

    @Override
    public void deleteItem(String itemID) {
//        DELETE ALL REVIEW REFERENCED TO THIS PARTICULAR ITEM
        List<String> reviewList = itemRepository.findAllReviewIDbyItemID(itemID);
        for (int i=0; i<reviewList.size(); i++){
            deleteReview(reviewList.get(i));
        }

        itemRepository.deleteById(itemID);
    }

    @Override
    public void deleteReview(String reviewID) {
        reviewRepository.deleteById(reviewID);
    }

    @Override
    public Item updateItem(Item item) {
        itemRepository.save(item);
        return item;
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        customerRepository.save(customer);
        return customer;
    }

    @Override
    public Review updateReview(Review review) {
        reviewRepository.save(review);
        return review;
    }

    @Override
    public Seller updateSeller(Seller seller) {
        sellerRepository.save(seller);
        return seller;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public List<Item> getAllItem() {
        return itemRepository.findAll();
    }

    @Override
    public List<Seller> getAllSeller() {
        return sellerRepository.findAll();
    }

    @Override
    public List<Review> getAllReview() {
        return reviewRepository.findAll();
    }

    @Override
    public List<Purchase> getAllPurchase(){
        return purchaseRepository.findAll();
    }

    @Override
    public Purchase updatePurchase(Purchase purchase){
        purchaseRepository.save(purchase);
        return purchase;
    }

    @Override
    public Purchase addPurchase(Purchase purchase){
        purchase.setSeller(purchase.getItemBought().getSeller());
        purchase.setTotalPrice(purchase.getQuantity() * purchase.getItemBought().getPrice());
        String sellerAbbreviation = purchase.getSeller().getUsername().substring(0, 3);
        String customerAbbreviation = purchase.getCustomer().getUsername().substring(0, 3);
        String itemID = purchase.getItemBought().getItemID();
        purchase.setPurchaseID(sellerAbbreviation + "-" + customerAbbreviation + "-" + itemID);
        purchase.setPaidStatus(false);
        purchaseRepository.save(purchase);
        return purchase;
    }

    @Override
    public Purchase purchaseSetPaidTrue(String id){
        Purchase purchase = purchaseRepository.getOne(id);
        try {
            purchase.setPaidStatus(true);
            purchaseRepository.save(purchase);
            return purchase;
        }
        catch (Exception e){
            return purchase;
        }
    }

    @Override
    public Purchase purchaseSetPaidFalse(String id){
        Purchase purchase = purchaseRepository.getOne(id);
        try {
            purchase.setPaidStatus(false);
            purchaseRepository.save(purchase);
            return purchase;
        }
        catch (Exception e){
            return purchase;
        }
    }

    @Override
    public void deletePurchase(String id){
        purchaseRepository.deleteById(id);
    }

    @Override
    public List<Purchase> getPurchaseFromCustomer(String customerID){
        return purchaseRepository.findAllPurchaseByCustomerID(customerID);
    }

    @Override
    public List<Purchase> getPurchaseFromSeller(String sellerID){
        return purchaseRepository.findAllPurchaseBySellerID(sellerID);
    }

    @Override
    public List<Promo> getAllPromo() {
        return promoRepository.findAll();
    }

    @Override
    public Promo addPromo(Promo promo) {
        promoRepository.save(promo);
        return promo;
    }

    @Override
    public Optional<Promo> getPromo(String promoID) {
        return promoRepository.findById(promoID);
    }

    @Override
    public Promo updatePromo(Promo promo) {
        promoRepository.save(promo);
        return promo;
    }

    @Override
    public void deletePromo(String id) {
        promoRepository.deleteById(id);
    }

    @Override
    public String getPromoDescriptionByPromoID(String id) {
        return promoRepository.getPromoDescriptionByPromoID(id);
    }


    @Override
    public List<Review> findAllReviewByItemId(String id) {
        return itemRepository.findAllReviewByItemID(id);
    }

    @Override
    public String getReviewDescriptionByReviewID(String id) {
        return reviewRepository.getReviewDescriptionByReviewID(id);
    }

    @Override
    public void subscribe(String idSeller, String idCustomer){
        Optional<Seller> seller = getSeller(idSeller);
        Optional<Customer> customer = getCustomer(idCustomer);
        if(seller.isPresent() && customer.isPresent()){
            System.out.println("jshFKJSHFKHFKJHSDFHDJFHJFKS");
            seller.get().getCustomers().add(customer.get());
            customer.get().getSellers().add(seller.get());
            updateCustomer(customer.get());
            updateSeller(seller.get());
        }
    }

    @Override
    public void notifyPromoToSubscriber(String idSeller, Promo promo){
        Seller seller = getSeller(idSeller).get();
        for(Customer customer : seller.getCustomers()){
            promo.addCustomerFromManyToMany(customer);
        }
        updatePromo(promo);
    }
    @Override
    public Set<Seller> getSubscribedSeller(String customerId){
        Customer customer = getCustomer(customerId).get();
        return customer.getSellers();
    }

    @Override
    public void unsubscribe(String idSeller, String idCustomer) {
        Optional<Seller> seller = getSeller(idSeller);
        Optional<Customer> customer = getCustomer(idCustomer);
        if(seller.isPresent() && customer.isPresent()){
            System.out.println("jshFKJSHFKHFKJHSDFHDJFHJFKS");
            seller.get().getCustomers().remove(customer.get());
            customer.get().getSellers().remove(seller.get());
            updateCustomer(customer.get());
            updateSeller(seller.get());
        }
    }

}

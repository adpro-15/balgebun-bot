package com.bot.balgebunbot.controller;

import com.bot.balgebunbot.service.ApplicationService;
import com.bot.balgebunbot.service.BotService;
import com.bot.balgebunbot.service.UniversalHandler;
import com.bot.balgebunbot.service.state.StateHelper;
import com.bot.balgebunbot.service.state.customer.CustomerState;
import com.bot.balgebunbot.service.state.seller.SellerState;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@LineMessageHandler
public class BalgebunBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private BotService botService;

    @Autowired
    private UniversalHandler universalHandler;

    @Autowired
    private StateHelper stateHelper;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
            throws ExecutionException, InterruptedException, ParseException{
        Source source = messageEvent.getSource();
        String replyToken = messageEvent.getReplyToken();

        String[] input = messageEvent.getMessage().getText().split(" ");

        String command = "";
        String argumentOne = "";
        String argumentTwo = "";
        String argumentThree = "";
        try {
            command = input[0].toLowerCase();
            argumentOne = input[1];
            argumentTwo = input[2];
            argumentThree = input[3];
        }
        catch(Exception e){
            System.out.println();
        }

        String userId = source.getUserId();
        String displayName = lineMessagingClient.getProfile(userId).get().getDisplayName();
        CustomerState customerState = stateHelper.getCustomerState(userId);
        SellerState sellerState = stateHelper.getSellerState(userId);

        if(command.equals("/register")) {
            if(argumentOne.equals("customer")){
                universalHandler.setResponses(customerState.register(argumentOne, argumentTwo, userId, displayName));
            }
            else if(argumentOne.equals("seller")){
                if(sellerState == stateHelper.sellerToState("SELLER UNREGISTERED1")) {
                    universalHandler.setResponses(sellerState.register(argumentOne, argumentTwo, userId, displayName));
                }
            }
            else if(sellerState == stateHelper.sellerToState("SELLER UNREGISTERED2")){
                universalHandler.setResponses(sellerState.register2(userId, argumentOne, argumentTwo));
            }
        }

        else if(customerState != stateHelper.getCustomerState("")) {
            switch (command) {
                case "/help":
                    universalHandler.setResponses(customerState.help());
                    break;

                case "/order":
                    universalHandler.setResponses(customerState.addOrder(userId, argumentOne, argumentTwo, argumentThree));
                    break;

                case "/remove":
                    universalHandler.setResponses(customerState.removeOrder(argumentOne));
                    break;

                case "/orderstatus":
                    universalHandler.setResponses(customerState.getOrderStatus(userId));
                    break;

                case "/history":
                    universalHandler.setResponses(customerState.getHistory(userId));
                    break;

                case "/subscribe":
                    universalHandler.setResponses(customerState.subscribe(userId, argumentOne));
                    break;

                case "/confirmorder":
                    universalHandler.setResponses(customerState.confirmOrder(userId));
                    break;

                case "/review":
                    universalHandler.setResponses(customerState.addReview(argumentOne, argumentTwo));
                    break;

                case "/pay":
                    universalHandler.setResponses(customerState.payOrder(userId));
                    break;

                case "/sellerlist":
                    universalHandler.setResponses(customerState.sellerList());
                    break;

                case "/item":
                    universalHandler.setResponses(customerState.itemList(argumentOne));
                    break;

                case "/accept":
                    universalHandler.setResponses(customerState.accept(userId));
                    break;

                case "/sublist":
                    universalHandler.setResponses(customerState.subList(userId));
                    break;

                case "/checkpromo":
                    universalHandler.setResponses(customerState.checkPromo(userId));
                    break;

                case "/unsub":
                    universalHandler.setResponses(customerState.unsubscribe(userId, argumentOne));
                    break;

                case "/getreview":
                    universalHandler.setResponses(customerState.getReview(userId, argumentOne));
                    break;

                default:
                    universalHandler.setResponses(customerState.invalid());
            }
        }

        else if(sellerState != stateHelper.getSellerState("")) {
            switch (command) {
                case "/help":
                    universalHandler.setResponses(sellerState.help());
                    break;

                case "/orders":
                    universalHandler.setResponses(sellerState.getOrders(userId));
                    break;

                case "/history":
                    universalHandler.setResponses(sellerState.getHistory(userId));
                    break;

                case "/addmenu":
                    universalHandler.setResponses(sellerState.addMenu(userId, argumentOne, argumentTwo));
                    break;

                case "/removemenu":
                    universalHandler.setResponses(sellerState.removeMenu(argumentOne));
                    break;

                case "/addpromo":
                    universalHandler.setResponses(sellerState.addPromo(userId, argumentOne));
                    break;

                case "/removepromo":
                    universalHandler.setResponses(sellerState.removePromo(argumentOne));
                    break;

                case "/getreview":
                    universalHandler.setResponses(sellerState.getReview(userId, argumentOne));
                    break;

                case "/cook":
                    universalHandler.setResponses(sellerState.cookOrder(argumentOne));
                    break;

                case "/done":
                    universalHandler.setResponses(sellerState.notifyOrderDone(argumentOne));
                    break;

                case "/menu":
                    universalHandler.setResponses(sellerState.menu(userId));
                    break;

                case "/checkpromo":
                    universalHandler.setResponses(sellerState.checkPromo(userId));
                    break;

                default:
                    universalHandler.setResponses(sellerState.invalid());
            }
        }
        else {
            universalHandler.setResponses(sellerState.help(userId));
        }
        applicationService.setHandler(universalHandler);
        List<Message> responseMessage = applicationService.getResponse();
        this.reply(replyToken, responseMessage);
    }

    private void reply(String replyToken, List<Message> message) {
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, message)).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}

package com.bot.balgebunbot.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "promo")
public class Promo implements Serializable {
    @Id
    @Column(name = "promo_id", updatable = false, nullable = false)
    private String promo_id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "username", nullable = false)
    private Seller seller;

    @Column(name = "promo_description")
    private String promo_description;

    @ManyToMany(cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "promo_customer",
            joinColumns = {
                    @JoinColumn(
                            name = "promo_id",
                            referencedColumnName = "promo_id"
                    )
            },
            inverseJoinColumns = {
                    @JoinColumn(
                            name = "customer_id",
                            referencedColumnName = "username"
                    )
            }
    )
    private Set<Customer> promo_customers = new HashSet<>();

    public Promo(){

    }
    public Promo (String promo_id, String promo_description, Seller seller){
        this.promo_id=promo_id;
        this.promo_description=promo_description;
        this.seller=seller;
    }

    public String getPromoID() {
        return promo_id;
    }

    public void setPromoID(String promo_id) {
        this.promo_id = promo_id;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public String getPromoDescription() {
        return promo_description;
    }

    public void addCustomerFromManyToMany(Customer customer){
        getPromo_customers().add(customer);
    }
    public void deleteCustomerFromManyToMany(Customer customer){
        getPromo_customers().remove(customer);
    }

    public Set<Customer> getPromo_customers() {
        return promo_customers;
    }

    public void setPromo_customers(Set<Customer> promo_customers) {
        this.promo_customers = promo_customers;
    }

    public void setPromoDescription(String promo_description) {
        this.promo_description = promo_description;
    }
}



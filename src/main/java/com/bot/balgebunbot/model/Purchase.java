package com.bot.balgebunbot.model;

import javax.persistence.*;

@Entity
@Table(name = "purchase")
public class Purchase {

    @Id
    @Column(name = "purchase_id", nullable = false)
    private String purchaseID;

    @Column(name = "total_price")
    private long totalPrice;

    @Column(name = "notes")
    private String notes;

    @Column(name = "state")
    private String state;

    @Column(name = "paid")
    private Boolean paidStatus;

    @Column(name = "quantity")
    private long quantity;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "counter_id", nullable = false)
    private Seller seller;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "username", nullable = false)
    private Customer customer;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "item_id", nullable = false)
    private Item itemBought;

    public Purchase(){}

    public Purchase(Seller seller, Customer customer, Item itemBought, Long quantity, String notes) {
        this.seller = seller;
        this.customer = customer;
        this.itemBought = itemBought;
        this.quantity = quantity;
        this.notes = notes;
        this.paidStatus = false;
        this.state = "ORDER UNACCEPTED";
    }

    public Purchase(String purchaseID){
        this.purchaseID = purchaseID;
    }

    public String getState(){
        return this.state;
    }

    public void setState(String state){
        this.state = state;
    }
    public Boolean getPaidStatus() {
        return this.paidStatus;
    }

    public void setPaidStatus(Boolean isPaid){
        this.paidStatus = isPaid;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setItemBought(Item itemBought) {
        this.itemBought = itemBought;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Seller getSeller(){return this.seller;}

    public long getTotalPrice() {
        return totalPrice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getPurchaseID() {
        return purchaseID;
    }

    public void setPurchaseID(String purchaseID) {
        this.purchaseID = purchaseID;
    }

    public Item getItemBought() {
        return itemBought;
    }

    public String getNotes() {
        return notes;
    }

    public void setSeller(Seller seller){
        this.seller = seller;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

}

package com.bot.balgebunbot.model;

import com.bot.balgebunbot.service.state.customer.CustomerRegisteredState;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "customer")
@JsonIdentityInfo(
        scope = Customer.class,
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "username")
public class Customer extends User implements Serializable {
    @ManyToMany(mappedBy = "customers",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Seller> sellers = new HashSet<>();

    @ManyToMany(mappedBy = "promo_customers",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Promo> promos = new HashSet<>();

    public Set<Promo> getPromos() {
        return promos;
    }

    public void setPromos(Set<Promo> promos) {
        this.promos = promos;
    }

    public Customer(){

    }

    public Customer(String username, String name, String phoneNumber){
        this.username = username;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.state = CustomerRegisteredState.DB_COL_NAME;
    }
    public void addPromo(Promo promo){
        getPromos().add(promo);
        promo.addCustomerFromManyToMany(this);
    }
    public void deletePromo(Promo promo){
        getPromos().remove(promo);
        promo.deleteCustomerFromManyToMany(this);
    }

    public Set<Seller> getSellers() {
        return sellers;
    }

    public void setSellers(Set<Seller> sellers) {
        this.sellers = sellers;
    }

}

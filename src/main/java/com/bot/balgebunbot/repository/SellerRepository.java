package com.bot.balgebunbot.repository;

import com.bot.balgebunbot.model.Promo;
import com.bot.balgebunbot.model.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SellerRepository extends JpaRepository<Seller, String>  {
    void deleteById(String id);

    Optional<Seller> findById(String id);

}

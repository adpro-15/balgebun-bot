package com.bot.balgebunbot.repository;

import com.bot.balgebunbot.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReviewRepository extends JpaRepository<Review, String> {
    @Query(value ="SELECT review_description FROM review WHERE review_id = ?1", nativeQuery = true)
    String getReviewDescriptionByReviewID (String id);
}

package com.bot.balgebunbot.repository;

import com.bot.balgebunbot.model.Promo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PromoRepository extends JpaRepository<Promo, String> {
    @Query(value ="SELECT promo_description FROM promo WHERE promo_id = ?1", nativeQuery = true)
    String getPromoDescriptionByPromoID (String id);

}

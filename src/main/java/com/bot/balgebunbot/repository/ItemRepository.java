package com.bot.balgebunbot.repository;

import com.bot.balgebunbot.model.Item;
import com.bot.balgebunbot.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, String> {
    @Query(value = "SELECT * FROM review where item_item_id = ?1", nativeQuery = true)
    List<Review> findAllReviewByItemID (String item_id);
    @Query(value = "SELECT review_id FROM review WHERE item_item_id = ?1", nativeQuery = true)
    List<String> findAllReviewIDbyItemID (String item_id) ;
}
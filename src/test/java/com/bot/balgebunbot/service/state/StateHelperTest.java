package com.bot.balgebunbot.service.state;

import com.bot.balgebunbot.service.state.customer.*;
import com.bot.balgebunbot.service.state.order.OrderAcceptedState;
import com.bot.balgebunbot.service.state.order.OrderCookingState;
import com.bot.balgebunbot.service.state.order.OrderDeliveredState;
import com.bot.balgebunbot.service.state.order.OrderFinishedState;
import com.bot.balgebunbot.service.state.seller.SellerAcceptingOrderState;
import com.bot.balgebunbot.service.state.seller.SellerRegisteredState;
import com.bot.balgebunbot.service.state.seller.SellerUnregisteredStatePhase1;
import com.bot.balgebunbot.service.state.seller.SellerUnregisteredStatePhase2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class StateHelperTest {

    @Autowired
    private CustomerRegisteredState customerRegisteredState;

    @Autowired
    private CustomerUnregisteredState customerUnregisteredState;

    @Autowired
    private CustomerConfirmedOrderState customerConfirmedOrderState;

    @Autowired
    private CustomerPayedState customerPayedState;

    @Autowired
    private CustomerOrderingState customerOrderingState;

    @Autowired
    private SellerRegisteredState sellerRegisteredState;

    @Autowired
    private SellerUnregisteredStatePhase1 sellerUnregisteredStatePhase1;

    @Autowired
    private SellerUnregisteredStatePhase2 sellerUnregisteredStatePhase2;

    @Autowired
    private SellerAcceptingOrderState sellerAcceptingOrderState;

    @Autowired
    private OrderFinishedState orderFinishedState;

    @Autowired
    private OrderDeliveredState orderDeliveredState;

    @Autowired
    private OrderCookingState orderCookingState;

    @Autowired
    private OrderAcceptedState orderAcceptedState;

    StateHelper stateHelper;
    @BeforeEach
    void setUp() {
        stateHelper = new StateHelper();
    }

    @Test
    void toState() {
        assertEquals(customerRegisteredState, stateHelper.customerToState(CustomerRegisteredState.DB_COL_NAME));

        assertEquals(customerOrderingState, stateHelper.customerToState(CustomerOrderingState.DB_COL_NAME));
        assertEquals(customerPayedState, stateHelper.customerToState(CustomerPayedState.DB_COL_NAME));
        assertEquals(customerUnregisteredState, stateHelper.customerToState(CustomerUnregisteredState.DB_COL_NAME));
        assertEquals(customerConfirmedOrderState, stateHelper.customerToState(CustomerConfirmedOrderState.DB_COL_NAME));

        assertEquals(sellerAcceptingOrderState, stateHelper.sellerToState(SellerAcceptingOrderState.DB_COL_NAME));
        assertEquals(sellerRegisteredState, stateHelper.sellerToState(SellerRegisteredState.DB_COL_NAME));
        assertEquals(sellerUnregisteredStatePhase1, stateHelper.sellerToState(SellerUnregisteredStatePhase1.DB_COL_NAME));
        assertEquals(sellerUnregisteredStatePhase2, stateHelper.sellerToState(SellerUnregisteredStatePhase2.DB_COL_NAME));

        assertEquals(orderAcceptedState, stateHelper.orderToState(OrderAcceptedState.DB_COL_NAME));
        assertEquals(orderCookingState, stateHelper.orderToState(OrderCookingState.DB_COL_NAME));
        assertEquals(orderDeliveredState, stateHelper.orderToState(OrderDeliveredState.DB_COL_NAME));
        assertEquals(orderFinishedState, stateHelper.orderToState(OrderFinishedState.DB_COL_NAME));

        assertNull(stateHelper.customerToState("test"));
        assertNull(stateHelper.sellerToState("test"));
        assertNull(stateHelper.orderToState("test"));
    }

//    @Test
//    void getCustomerState() {
//        assertEquals(customerUnregisteredState, stateHelper.getCustomerState("1"));
//    }
//
//    @Test
//    void getSellerState() {
//        assertEquals(sellerUnregisteredState, stateHelper.getSellerState("2"));
//    }
}
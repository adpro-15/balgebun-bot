package com.bot.balgebunbot.service;

import com.bot.balgebunbot.model.*;
import com.bot.balgebunbot.repository.*;
import com.bot.balgebunbot.service.BotServiceImpl;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.provider.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class BotServiceImplTest {
    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private SellerRepository sellerRepository;

    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private PromoRepository promoRepository;

    @Mock
    private PurchaseRepository purchaseRepository;

    @InjectMocks
    private BotServiceImpl botService;

    Customer customer;
    Seller seller;
    Item item;
    Review review;

    Promo promo;

    Purchase purchase;
    Set<Item> itemp1;

    @BeforeEach
    public void setUp(){
        customer = new Customer("mrifqyz17", "cinoy", "087777535288");
        seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        item = new Item(1, "NGP", "Nasi Goreng Pedas", 12000, seller);
        review = new Review("R01", "Jelek", item);
        promo = new Promo("1","BUY 1 GET 1", seller);
        purchase = new Purchase(seller, customer, item, (long) 3, "notes");

    }

    @Test
    public void testCustomerRepositoryAddCustomer(){
        botService.addCustomer(customer);
        lenient().when(botService.addCustomer(customer)).thenReturn(customer);
    }

    @Test
    public void testSellerRepositoryAddSeller(){
        botService.addSeller(seller);
        lenient().when(botService.addSeller(seller)).thenReturn(seller);
    }

    @Test
    public void testItemRepositoryAddItem(){
        botService.addItem(item);
        lenient().when(botService.addItem(item)).thenReturn(item);
    }

    @Test
    public void testReviewRepositoryAddReview(){
        botService.addReview(review);
        lenient().when(botService.addReview(review)).thenReturn(review);
    }

    @Test
    public void testPurchaseRepositoryAddPurchase(){
        botService.addPurchase(purchase);
        lenient().when(botService.addPurchase(purchase)).thenReturn(purchase);
    }

    @Test
    public void testCustomerRepositoryGetCustomer(){
        botService.addCustomer(customer);
        lenient().when(botService.getCustomer("mrifqyz17")).thenReturn(java.util.Optional.ofNullable(customer));
    }

    @Test
    public void testSellerRepositoryGetSeller(){
        botService.addSeller(seller);
        lenient().when(botService.getSeller("subur")).thenReturn(java.util.Optional.ofNullable(seller));
    }

    @Test
    public void testItemRepositoryGetItem(){
        botService.addItem(item);
        lenient().when(botService.getItem("NGP")).thenReturn(java.util.Optional.ofNullable(item));
    }

    @Test
    public void testReviewRepositoryGetReview(){
        botService.addReview(review);
        lenient().when(botService.getReview("R01")).thenReturn(java.util.Optional.ofNullable(review));
    }

    @Test
    public void testPurchaseRepositoryGetPurchase(){
        botService.addPurchase(purchase);
        lenient().when(botService.getPurchase("1")).thenReturn(java.util.Optional.ofNullable(purchase));
    }

    @Test
    public void testCustomerRepositoryGetAllCustomer(){
        botService.addCustomer(customer);
        List<Customer> customerList = botService.getAllCustomer();
        lenient().when(botService.getAllCustomer()).thenReturn(customerList);
    }

    @Test
    public void testSellerRepositoryGetAllSeller(){
        botService.addSeller(seller);
        List<Seller> sellerList = botService.getAllSeller();
        lenient().when(botService.getAllSeller()).thenReturn(sellerList);
    }

    @Test
    public void testItemRepositoryGetAllItem(){
        botService.addItem(item);
        List<Item> itemList = botService.getAllItem();
        lenient().when(botService.getAllItem()).thenReturn(itemList);
    }

    @Test
    public void testReviewRepositoryGetAllReview(){
        botService.addReview(review);
        List<Review> reviewList = botService.getAllReview();
        lenient().when(botService.getAllReview()).thenReturn(reviewList);
    }

    @Test
    public void testPurchaseRepositoryGetAllPurchase(){
        botService.addPurchase(purchase);
        List<Purchase> purchaseList = botService.getAllPurchase();
        lenient().when(botService.getAllPurchase()).thenReturn(purchaseList);
    }

    @Test
    public void testCustomerRepositoryDeleteCustomer(){
        botService.addCustomer(customer);
        botService.deleteCustomer("mrifqyz17");
        lenient().when(botService.getCustomer("mrifqyz17")).thenReturn(null);
    }

    @Test
    public void testSellerRepositoryDeleteSeller(){
        botService.addSeller(seller);
        botService.deleteSeller("subur");
        lenient().when(botService.getSeller("subur")).thenReturn(null);
    }

    @Test
    public void testItemRepositoryDeleteItem(){
        botService.addItem(item);
        botService.deleteItem("NGP");
        lenient().when(botService.getItem("NGP")).thenReturn(null);
    }

    @Test
    public void testReviewRepositoryDeleteReview(){
        botService.addReview(review);
        botService.deleteReview("R01");
        lenient().when(botService.getReview("R01")).thenReturn(null);
    }

    @Test
    public void testPurchaseRepositoryDeletePurchase(){
        botService.addPurchase(purchase);
        purchase.setPurchaseID("1");
        botService.deletePurchase("1");
        lenient().when(botService.getPurchase("1")).thenReturn(null);
    }

    @Test
    public void testCustomerRepositoryUpdateCustomer(){
        botService.addCustomer(customer);
        customer = new Customer("mrifqyz17", "aa enoy", "087777535288");
        lenient().when(botService.updateCustomer(customer)).thenReturn(customer);
    }

    @Test
    public void testSellerRepositoryUpdateSeller(){
        botService.addSeller(seller);
        seller = new Seller("subur", "minum jamu", "14045170700",
                "SAW", "14045170700");
        lenient().when(botService.updateSeller(seller)).thenReturn(seller);
    }

    @Test
    public void testItemRepositoryUpdateItem(){
        botService.addItem(item);
        item = new Item(1, "NGP", "Nasi Goreng Pete", 15000, seller);
        lenient().when(botService.updateItem(item)).thenReturn(item);
    }

    @Test
    public void testReviewRepositoryUpdateReview(){
        botService.addReview(review);
        review = new Review("R01", "Bagus", item);
        lenient().when(botService.updateReview(review)).thenReturn(review);
    }

    @Test
    void getAllPromo() {
        botService.addPromo(promo);
        List <Promo> promos = botService.getAllPromo();
        lenient().when(botService.getAllPromo()).thenReturn(promos);
    }

    @Test
    void addPromo() {
        botService.addPromo(promo);
        lenient().when(botService.addPromo(promo)).thenReturn(promo);
    }

    @Test
    void getPromo() {
        botService.addPromo(promo);
        lenient().when(botService.getPromo("1")).thenReturn(java.util.Optional.ofNullable(promo));
    }

    @Test
    void updatePromo() {
        botService.addPromo(promo);
        promo = new Promo("2","BUY 2 GET 2", seller);
        lenient().when(botService.updatePromo(promo)).thenReturn(promo);
    }

    @Test
    void deletePromo() {
        botService.addPromo(promo);
        botService.deletePromo("1");
        lenient().when(botService.getPromo("1")).thenReturn(null);
    }

    @Test
    void getPromoDescriptionByPromoID() {
        botService.addPromo(promo);
        lenient().when(botService.getPromoDescriptionByPromoID("1")).thenReturn("BUY 1 GET 1");
    }

    @Test
    void getReviewDescriptionByReviewID() {
        lenient().when(botService.getReviewDescriptionByReviewID("R01")).thenReturn("Jelek");
    }

    @Test
    void findAllReviewByItemId() {
        List<Review> reviews = new ArrayList<Review>();
        reviews.add(review);
        lenient().when(botService.findAllReviewByItemId("NGP")).thenReturn(reviews);
    }

    public void testPurchaseRepositoryUpdaetPurchase(){
        botService.addPurchase(purchase);
        purchase = new Purchase(seller, customer, item, (long) 3, "notes1");
        lenient().when(botService.updatePurchase(purchase)).thenReturn(purchase);
    }

}

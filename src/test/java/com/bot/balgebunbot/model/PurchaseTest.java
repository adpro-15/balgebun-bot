package com.bot.balgebunbot.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PurchaseTest {

    Seller s1, s2;
    Customer c1, c2;
    Item is11, is12, is21;
    Purchase p1, p2, p3;

    @BeforeEach
    public void setUp(){
        s1 = new Seller("s1", "ns1", "12", "1", "12");
        s2 = new Seller("s2", "ns2", "22", "2", "22");
        c1 = new Customer("c1", "nc1", "11");
        c2 = new Customer("c2", "nc2", "21");
        is11 = new Item(11, "is11", "item1s1", 69, s1);
        is12 = new Item(12, "is12", "item2s1", 70, s1);
        is21 = new Item(21, "is21", "item1s2", 4, s2);
        p1 = new Purchase(s1, c1, is11, (long)2,"notesp1");
        p1.setTotalPrice(is11.getPrice()*2);
        p2 = new Purchase(s1, c2, is12,(long)3,  "notesp2");
        p2.setTotalPrice(is12.getPrice()*3);
        p3 = new Purchase(s2, c1, is21, (long)3, "notesp3");
        p3.setTotalPrice(is21.getPrice()*3);
    }

    @Test
    void getSellerReturnsTheRightSeller(){
        assertEquals(s1, p1.getSeller());
        assertEquals(s1, p2.getSeller());
        assertEquals(s2, p3.getSeller());
    }

    @Test
    void totalPriceShouldAddForEveryItem(){
        assertEquals(p1.getTotalPrice(), 138);
        assertEquals(p2.getTotalPrice(), 210);
        assertEquals(p3.getTotalPrice(), 12);
    }

    @Test
    void setItemBought(){
        p1.setItemBought(is21);
        assertEquals(p1.getItemBought(), is21);
    }

    @Test
    void getAndSetPurchaseIDTest(){
        p1.setPurchaseID("123123");
        assertEquals(p1.getPurchaseID(), "123123");
    }

    @Test
    void setPurchaseTotalPriceTest(){
        p1.setTotalPrice(1000);
        assertEquals(p1.getTotalPrice(), 1000);
    }

    @Test
    void setAndGetQuantityTest(){
        p1.setQuantity(7);
        assertEquals(p1.getQuantity(), 7);
    }

    @Test
    void setAndGetNotesTest(){
        p1.setNotes("Halo");
        assertEquals(p1.getNotes(), "Halo");
    }




}

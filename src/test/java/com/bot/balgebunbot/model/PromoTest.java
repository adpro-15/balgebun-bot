package com.bot.balgebunbot.model;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PromoTest {

    @Test
    void getPromoDescription() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        Promo nullPromo = new Promo();
        assertEquals(promo.getPromoDescription(),"BUY 1 GET 1");
        assertEquals(nullPromo.getPromoDescription(),null);
    }

    @Test
    void setPromoDescription() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        promo.setPromoDescription("BUY 1 GET 2");
        assertEquals(promo.getPromoDescription(),"BUY 1 GET 2");
    }

    @Test
    void getPromoID() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        assertEquals(promo.getPromoID(),"1");
    }

    @Test
    void setPromoID() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        promo.setPromoID("11");
        assertEquals(promo.getPromoID(),"11");
    }

    @Test
    void getSeller() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        assertEquals(promo.getSeller(),seller);
    }

    @Test
    void setSeller() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        Seller seller2 = new Seller("bubur", "ayam", "081208120812", "BA", "081208120812");
        promo.setSeller(seller2);
        assertEquals(promo.getSeller(),seller2);

    }


    @Test
    void setPromo_customers() {
        Seller seller = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        Promo promo = new Promo("1", "BUY 1 GET 1", seller);
        Set<Customer> test = new HashSet<>();
        Customer c =  new Customer("mrifqyz17", "cinoy", "087777535288");
        test.add(c);
        promo.setPromo_customers(test);
        assertEquals(promo.getPromo_customers(),test);
    }
}
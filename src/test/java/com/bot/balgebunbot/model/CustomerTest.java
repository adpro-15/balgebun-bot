package com.bot.balgebunbot.model;

import com.bot.balgebunbot.model.Customer;
import com.bot.balgebunbot.model.Seller;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerTest {
    Customer customer;
    Customer customerWithNoArgsConstructor;
    Seller sellerDummy;
    Promo promo;

    @BeforeEach
    public void setUp(){
        customer = new Customer("mrifqyz17", "cinoy", "087777535288");
        customerWithNoArgsConstructor = new Customer();
        sellerDummy = new Seller("subur", "arya wiguna", "14045170700",
                "SAW", "14045170700");
        promo = new Promo("1", "BUY 1 GET 1", sellerDummy);
    }

    @Test
    public void testGetterMethod(){
        Set<Seller> sellersTest = customer.getSellers();
        Set<Promo> promoTest = customer.getPromos();
        assertEquals(customer.getName(), "cinoy");
        assertEquals(customer.getPhoneNumber(), "087777535288");
        assertEquals(customer.getUsername(), "mrifqyz17");
        assertEquals(customer.getState(), "CUSTOMER REGISTERED");
        assertNull(customerWithNoArgsConstructor.getUsername());
        assertTrue(sellersTest.isEmpty());
        assertTrue(promoTest.isEmpty());

    }

    @Test
    public void testSetterMethod(){
        Set<Seller> setDummy = new HashSet<>();
        Set<Promo> setPromoDummy = new HashSet<>();
        setPromoDummy.add(promo);
        setDummy.add(sellerDummy);
        customer.setName("Cicak bin Kadal");
        customer.setPhoneNumber("180050050517");
        customer.setState("Ordering");
        customer.setUsername("cibikal");
        customer.setSellers(setDummy);
        customer.setPromos(setPromoDummy);



        assertEquals(customer.getName(), "Cicak bin Kadal");
        assertEquals(customer.getPhoneNumber(), "180050050517");
        assertEquals(customer.getUsername(), "cibikal");
        assertEquals(customer.getState(), "Ordering");
        assertFalse(customer.getSellers().isEmpty());
        assertFalse(customer.getPromos().isEmpty());
    }


    @Test
    void addAndDeletePromo() {
        customer.addPromo(promo);
        assertEquals(customer.getPromos().size(), 1);
        customer.deletePromo(promo);
        assertEquals(customer.getPromos().size(),0);
    }
}